package com.example.motrackcodechallenge.view.ui

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.example.motrackcodechallenge.data.model.Movie
import com.example.motrackcodechallenge.databinding.FragmentMainBinding
import com.example.motrackcodechallenge.utills.autoCleared
import com.example.motrackcodechallenge.view.adapter.MovieAdapter
import com.example.motrackcodechallenge.viewModel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class MainFragment : Fragment() {
    private var binding: FragmentMainBinding by autoCleared()
    private val viewModel: MainViewModel by viewModels()
    private lateinit var moviesAdapter: MovieAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        intRecyclerView()
        collectUiState()
        intiView()
    }

    private fun intRecyclerView() {
        moviesAdapter = MovieAdapter { position -> onItemClicked(position) }
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.recyclerView.adapter = moviesAdapter
    }

    private fun intiView() {
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                var month = monthOfYear.inc()
                var date = if (monthOfYear < 10)
                    "$year-0$month-$dayOfMonth"
                else
                    "$year-0$month-$dayOfMonth"

                filterMoviesByDate(date)
            }
        var cal = Calendar.getInstance()
        binding?.searchView.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private fun filterMoviesByDate(date: String?) {
    }

    private fun onItemClicked(movieID: Long) {
        val action =
            MainFragmentDirections
                .actionMainFragmentToMovieDetailFragment(movieID)
        findNavController().navigate(action)
    }

    private fun collectUiState() {
        lifecycleScope.launch {
            viewModel.fetchData().collectLatest {
                    movies -> moviesAdapter?.submitData(movies)
            }
        }
        lifecycleScope.launch {
            moviesAdapter.loadStateFlow.collectLatest { loadStates ->
                binding.pgBar.isVisible = loadStates.refresh is LoadState.Loading
               // retry.isVisible = loadState.refresh !is LoadState.Loading
              //  binding.isVisible = loadState.refresh is LoadState.Error
            }
        }
    }
}


