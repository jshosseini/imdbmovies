package com.example.motrackcodechallenge.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.motrackcodechallenge.R
import com.example.motrackcodechallenge.data.model.MovieDetail
import com.example.motrackcodechallenge.databinding.FragmentMovieDetailBinding
import com.example.motrackcodechallenge.utills.Resource
import com.example.motrackcodechallenge.utills.autoCleared
import com.example.motrackcodechallenge.viewModel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailFragment : Fragment() {

    private var binding: FragmentMovieDetailBinding by autoCleared()
    private val viewModel: MovieDetailViewModel by viewModels()

    private var movieID: Long? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movieID = it.getLong("movieID")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMovieId()
        setupObserver()
    }

    private fun setMovieId() {
        movieID?.let { viewModel.setMovieId(it) }
    }

    private fun setupObserver() {
        viewModel.movieDetail.observe(viewLifecycleOwner)
        {
            when (it) {
                is Resource.Success -> it.data?.let { it1 -> setModel(it1)
                binding.pgBar.visibility=View.GONE}
                is Resource.Error -> it.message?.let { it1 -> showMessageError(it1) }
            }
        }
    }

    private fun setModel(movieDetail: MovieDetail)
    {
        Glide.with(binding.root)
            .load("https://www.themoviedb.org/t/p/w600_and_h900_bestv2/${movieDetail.backdropPath}")
            .into(binding.detailMovieImg)
        binding.detailProgressCircular.max=10
        binding.detailProgressCircular.progress = movieDetail.voteAverage.toInt()
        binding.detailTitleTv.text=movieDetail.title
        binding.detailCreatedAtTv.text=movieDetail.releaseDate
        binding.detailScoreTv.text=context?.resources?.getString(R.string.scroe)+"=${movieDetail.budget}"
        binding.homePageTv.text=context?.resources?.getString(R.string.homePge)+"=${movieDetail.homepage}"
        binding.imdbTv.text=context?.resources?.getString(R.string.imdbId)+":${movieDetail.imdbID}"
        binding.overviewTv.text=context?.resources?.getString(R.string.overView)+":${movieDetail.overview}"
        binding.originalTitleTv.text=context?.resources?.getString(R.string.origintitle)+"=${movieDetail.originalTitle}"
        binding.originalTitleTv.text=context?.resources?.getString(R.string.originalLanguage)+"=${movieDetail.originalLanguage}"
        binding.detailGenreTv.text=context?.resources?.getString(R.string.gener)+"=${movieDetail.genres[0].name}"

    }

    private fun showMessageError(errorString: String) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }
}