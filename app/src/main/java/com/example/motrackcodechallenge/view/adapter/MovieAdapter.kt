package com.example.motrackcodechallenge.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.motrackcodechallenge.data.model.Movie
import com.example.motrackcodechallenge.databinding.MovieItemBinding

    class MovieAdapter(private val onItemClicked: (movieId: Long) -> Unit): PagingDataAdapter<Movie, ViewHolder>(MovieDiffCallBack()){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            MovieItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ),onItemClicked
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }
    }

    class MovieDiffCallBack : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }
    }
     class ViewHolder(private var item: MovieItemBinding,private val onItemClicked: (movieId: Long) -> Unit) : RecyclerView.ViewHolder(item.root), View.OnClickListener
     {

        init {
            itemView.setOnClickListener(this)
        }
        fun bind(movie: Movie) {
            item.root.tag=movie.id
            itemView.tag=movie.id
            item.model=movie
            item.progressCircular.max=10
            item.progressCircular.progress = movie.voteAverage.toInt()
            var url="https://www.themoviedb.org/${movie.id}/$"
            Glide.with(item.root)
                .load("https://www.themoviedb.org/t/p/w600_and_h900_bestv2/${movie.backdropPath}")
                .into(item.movieImg)
        }

        override fun onClick(view: View?) {
            onItemClicked(view?.tag as Long) }

}