package com.example.motrackcodechallenge.data.model

import com.google.gson.annotations.SerializedName

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class Date(
    @SerializedName("maximum") val maximum: String,
    @SerializedName("minimum") var minimum: String
)