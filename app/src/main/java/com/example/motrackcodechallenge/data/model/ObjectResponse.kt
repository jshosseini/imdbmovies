package com.example.motrackcodechallenge.data.model

import com.google.gson.annotations.SerializedName

data class ObjectResponse<T> (
    @SerializedName("success") val success: Boolean,
    @SerializedName("message") val message: String,
    @SerializedName("data") val result: T
        )