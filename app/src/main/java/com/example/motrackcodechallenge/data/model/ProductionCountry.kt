package com.example.motrackcodechallenge.data.model

import com.google.gson.annotations.SerializedName

data class ProductionCountry(
    val id: Long,

    @SerializedName("logo_path")
    val logoPath: String,

    val name: String,

    @SerializedName("origin_country")
    val originCountry: String
)
