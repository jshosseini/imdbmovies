package com.example.motrackcodechallenge.data.dataSource

import com.example.motrackcodechallenge.utills.Resource

abstract class BaseDataSource {
    protected suspend fun <T> getObjectResponseResult(call: suspend () -> T): Resource<T> {

        return try {
            val response = call()
                if (response != null) Resource.Success(response)
                else Resource.Error("isEmpty")

        } catch (e: Exception) {
            Resource.Error(e.toString())
        }
    }
}


