package com.example.motrackcodechallenge.data.dataSource

import okhttp3.*
import javax.inject.Inject


class HeaderInterceptorImpl @Inject constructor() :Interceptor
{
    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val originalHttpUrl: HttpUrl = original.url
        val url = originalHttpUrl.newBuilder().build()
        val requestBuilder: Request.Builder = original.newBuilder()
//              .addHeader("api_key", "0b4da72b3a9254b665dbda27920c23df")
//            .addHeader("Content-Type", "application/json")
//            .addHeader("Authorization", "Bearer $token")
            .url(url)
        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }
}