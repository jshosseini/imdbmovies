package com.example.motrackcodechallenge.data.repository
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.motrackcodechallenge.data.dataSource.BaseDataSource
import com.example.motrackcodechallenge.data.dataSource.MoviesPagingSource
import com.example.motrackcodechallenge.data.dataSource.RequestApi
import com.example.motrackcodechallenge.data.model.*
import com.example.motrackcodechallenge.utills.Resource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
const val NETWORK_PAGE_SIZE = 25
class Repository @Inject constructor(
    private val requestApi: RequestApi ) : BaseDataSource()
{
     fun getMovies(): Flow<PagingData<Movie>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                MoviesPagingSource(service = requestApi)
            }
        ).flow
    }

    suspend fun getMovieDetailById(id:Long):Resource<MovieDetail>
    {
        return getObjectResponseResult{ requestApi.getDetailMovieById(id) }
    }
}