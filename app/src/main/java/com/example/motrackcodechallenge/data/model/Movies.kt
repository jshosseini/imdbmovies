package com.example.motrackcodechallenge.data.model

import com.google.gson.annotations.SerializedName

data class Movies(
    @SerializedName("dates") val date: Date,
    @SerializedName("page") val page: Int,
    @SerializedName("results") val movies: List<Movie>,
    @SerializedName("total_pages") val totalPages:String,
    @SerializedName("total_results") val totalResults: String
)

