package com.example.motrackcodechallenge.data.dataSource

import com.example.motrackcodechallenge.data.model.*
import retrofit2.http.*

interface RequestApi {

     @GET("movie/popular?api_key=5bf9c13ed868ac520f74e5095a83db0a")
     suspend fun getMovies( @Query("page") page: Int ,@Query("language") language: String):Movies

     @GET("movie/{id}?api_key=5bf9c13ed868ac520f74e5095a83db0a")
     suspend fun getDetailMovieById(@Path("id") id: Long): MovieDetail

}