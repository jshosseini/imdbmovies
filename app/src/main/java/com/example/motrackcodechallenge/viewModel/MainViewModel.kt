package com.example.motrackcodechallenge.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.motrackcodechallenge.data.model.Movie
import com.example.motrackcodechallenge.data.repository.Repository
import com.example.motrackcodechallenge.utills.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    init {
        fetchData()
    }

    fun fetchData(): Flow<PagingData<Movie>> {
        return repository.getMovies()
            .cachedIn(viewModelScope)
    }
}