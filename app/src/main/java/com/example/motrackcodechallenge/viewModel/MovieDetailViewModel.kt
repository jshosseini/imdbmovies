package com.example.motrackcodechallenge.viewModel

import androidx.lifecycle.*
import com.example.motrackcodechallenge.data.model.MovieDetail
import com.example.motrackcodechallenge.data.repository.Repository
import com.example.motrackcodechallenge.utills.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val _movieId = MutableLiveData<Long>()

    private val _movieDetail = _movieId.switchMap { movieId ->
        liveData<Resource<MovieDetail>> { emit(repository.getMovieDetailById(movieId)) }
    }
    val movieDetail: LiveData<Resource<MovieDetail>> get() = _movieDetail

    fun setMovieId(id:Long)
    {
        _movieId.value=id
    }
}